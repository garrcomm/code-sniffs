# Custom code sniffs by Stefan Thoolen

This package contains some additional PHP Codesniffer rules that can be included in your project.

## Usage

These sniffs can be added as a composer package;

```shell
$ composer require garrcomm/code-sniffs dev-master --dev
```

After installing the package, add the following line to to your `phpcs.xml` file:

```xml
<config name="installed_paths" value="vendor/garrcomm/code-sniffs"/>
<rule ref="Garrcomm"/>
```

Or run PHP Codesniffer with the following parameters:

```shell
$ ./vendor/bin/phpcs --standard=vendor/garrcomm/code-sniffs path/to/your/files
```

## But why don't you just write clean code from the start?

Sniffs help clean up legacy code, it's not just restricting new code.
I found a few issues in existing legacy code and wrote sniffs to detect (and repair) some common issues.

## Code quality

These sniffs are also written in PHP.
Those PHP files are scanned with PHPUnit and PHPStan to make sure they work properly.
Also, PHP CodeSniffer itself scanned the code so it complies with the PSR-12 standards and are well-documented.
Those scans are automated in [Bitbucket Pipelines](https://bitbucket.org/garrcomm/code-sniffs/pipelines).

## Available sniffs

* [Garrcomm.Classes.ClassDeclarationInString.Found](Garrcomm/Docs/Classes/ClassDeclarationInStringMarkDown.md)  
  Replaces class names as string (`$em->getReference('Foo\Bar', 1)`) to `::class` constants (`$em->getReference(\Foo\Bar::class, 1)`).
* [Garrcomm.Commenting.DisallowCommentedCode.Found](Garrcomm/Docs/Commenting/DisallowCommentedCodeMarkDown.md)  
  Errors on code that's commented out (example: `// var_dump($foo);`)
* [Garrcomm.Commenting.DisallowHashComments.Found](Garrcomm/Docs/Commenting/DisallowHashCommentsMarkDown.md)  
  Replaces hash comments (`# perl style comment`) to regular comments (`// regular comment`)
* [Garrcomm.Namespaces.UnusedUses.Found](Garrcomm/Docs/Namespaces/UnusedUsesMarkDown.md)  
  Removes unused use statements (for example `use Foo\Bar;` when `Bar` is not in use in the file)

## Tip for Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpstan.bat](bin/phpstan.bat), [bin\phpunit.bat](bin/phpunit.bat),
[bin\phpcs.bat](bin/phpcs.bat) and [bin\phpcbf.bat](bin/phpcbf.bat) as shortcuts for Composer, PHP Static Analyser, PHP Unit, CodeSniffer and Code Beautifier without the need of installing PHP and other dependencies on your machine.
