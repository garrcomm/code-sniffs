<?php

namespace Garrcomm\Sniffs\Namespaces;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class UnusedUsesSniff implements Sniff
{
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return int[]
     */
    public function register()
    {
        return array(T_USE);
    }

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param File    $phpcsFile The file where the token was found.
     * @param integer $stackPtr  The position in the stack where
     *                           the token was found.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $result = $this->getUseStatements($phpcsFile, $stackPtr);
        if ($result === null) {
            return;
        }
        foreach ($result['statements'] as $useStatement) {
            if (
                !$this->useStatementInUse($phpcsFile, (string)$useStatement['alias'], (int)$result['endOfStatement'])
            ) {
                $error = 'Use statement not in use: %s => %s';
                $data = array($useStatement['FQCN'], $useStatement['alias']);
                if ($phpcsFile->addError($error, $stackPtr, 'Found', $data, 0, true)) {
                    for (
                        $stackPtrIterator = $stackPtr;
                        $stackPtrIterator <= $result['endOfStatement'];
                        ++$stackPtrIterator
                    ) {
                        $phpcsFile->fixer->replaceToken($stackPtrIterator, '');
                    }
                }
            }
        }
    }

    /**
     * Checks if a specific use statement alias is in use in code
     *
     * @param File    $phpcsFile    The file where the current token is found.
     * @param string  $alias        Full name of the token we search for.
     * @param integer $fromStackPtr Search from this stack pointer.
     *
     * @return boolean
     */
    private function useStatementInUse(File $phpcsFile, string $alias, int $fromStackPtr): bool
    {
        $tokens = $phpcsFile->getTokens();
        for ($stackPtr = $fromStackPtr; $stackPtr < count($tokens); ++$stackPtr) {
            if ($stackPtr <= $fromStackPtr) {
                continue;
            }
            if ($tokens[$stackPtr]['content'] !== $alias) {
                continue;
            }
            if ($tokens[$stackPtr]['type'] == 'T_STRING') {
                if ($alias == $this->getFullName($phpcsFile, $stackPtr)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Gets the full name of a class
     *
     * Example:
     *
     * echo Foo\Bar::Baz();
     * When $stackPtr refers to Bar, this function will return Foo\Bar
     *
     * @param File    $phpcsFile The file where the current token is found.
     * @param integer $stackPtr  Position of the name.
     *
     * @return string
     */
    private function getFullName(File $phpcsFile, int $stackPtr): string
    {
        $tokens = $phpcsFile->getTokens();

        $return = '';
        while ($stackPtr > 0 && in_array($tokens[$stackPtr]['type'], ['T_STRING', 'T_NS_SEPARATOR'])) {
            $return = $tokens[$stackPtr]['content'] . $return;
            --$stackPtr;
        }

        return $return;
    }

    /**
     * Fetches the use statement and returns an array with keys `FQCN`, `alias` and `endOfStatement`
     *
     * @param File    $phpcsFile Reference to the PHP script we're processing.
     * @param integer $stackPtr  Pointer in the stack.
     *
     * @return array<string, int|string>|null
     */
    private function getUseStatements(File $phpcsFile, int $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $endOfStatement = $phpcsFile->findEndOfStatement($stackPtr, T_COMMA);

        // Parse the `use` statement
        $return = [];
        $FQCN = '';
        $asParsed = false;
        $alias = '';
        for ($stackPtrIterator = $stackPtr; $stackPtrIterator <= $endOfStatement; ++$stackPtrIterator) {
            switch ($tokens[$stackPtrIterator]['type']) {
                case 'T_USE':
                    // Ignore; this is the trigger for the process() function already
                    break;
                case 'T_AS':
                    $asParsed = true;
                    break;
                case 'T_WHITESPACE':
                    // Whitespaces are used as separators in code but can be ignored for parsing the use statement
                    break;
                case 'T_STRING':
                case 'T_NS_SEPARATOR':
                    // Strings and namespace separators are part of the class name
                    if ($asParsed) {
                        $alias .= $tokens[$stackPtrIterator]['content'];
                    } else {
                        $FQCN .= $tokens[$stackPtrIterator]['content'];
                    }
                    break;
                case 'T_COMMA':
                    // More then one use statement in this stack, go parse the next
                    $return[] = [
                        'FQCN' => $FQCN,
                        'alias' => $alias,
                    ];
                    $FQCN = '';
                    $asParsed = false;
                    $alias = '';
                    break;
                case 'T_SEMICOLON':
                    // We're at the end of the final use statement
                    $return[] = [
                        'FQCN' => $FQCN,
                        'alias' => $alias,
                    ];
                    $FQCN = '';
                    $alias = '';
                    break 2;
                case 'T_OPEN_PARENTHESIS':
                    /**
                     * A `use` statement can also be used with anonymous functions, like:
                     * foo(function($bar) use ($baz) { echo $bar . $baz; });
                     * In those cases, we just return
                     */
                    return null;
                case 'T_OPEN_CURLY_BRACKET':
                    /**
                     * A `use` statement can also be used to import a trait.
                     * While importing traits, it's possible to alias functions, like:
                     * use Foo\TraitClass { method1 as methoda; }
                     */
                    return null;
                default:
                    throw new \RuntimeException(
                        'Unexpected token type ' . $tokens[$stackPtrIterator]['type'] . ' found'
                    );
            }
        }

        // By default, the alias is the same as the class name
        foreach ($return as &$set) {
            if ($set['alias'] == '') {
                $set['alias'] = array_reverse(explode('\\', $set['FQCN']))[0];
            }
        }

        return [
            'statements' => $return,
            'endOfStatement' => $endOfStatement,
        ];
    }
}
