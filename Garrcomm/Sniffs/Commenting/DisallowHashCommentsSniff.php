<?php

/**
 * This sniff prohibits the use of Perl style hash comments.
 */

namespace Garrcomm\Sniffs\Commenting;

use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Files\File;

/**
 * This sniff prohibits the use of Perl style hash comments.
 *
 * An example of a hash comment is:
 *
 * <code>
 *  # This is a hash comment, which is prohibited.
 *  $hello = 'hello';
 * </code>
 */
final class DisallowHashCommentsSniff implements Sniff
{
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return int[]
     */
    public function register()
    {
        return array(T_COMMENT);
    }

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param File    $phpcsFile The file where the token was found.
     * @param integer $stackPtr  The position in the stack where
     *                           the token was found.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        if (substr($tokens[$stackPtr]['content'], 0, 1) === '#') {
            $error = 'Hash comments are prohibited; found %s';
            $data = array(trim($tokens[$stackPtr]['content']));
            if ($phpcsFile->addError($error, $stackPtr, 'Found', $data, 0, true)) {
                $fix = '//' . substr($tokens[$stackPtr]['content'], 1);
                $phpcsFile->fixer->replaceToken($stackPtr, $fix);
            }
        }
    }
}
