<?php

namespace Garrcomm\Sniffs\Commenting;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class DisallowCommentedCodeSniff implements Sniff
{
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return int[]
     */
    public function register()
    {
        return array(T_COMMENT);
    }

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param File    $phpcsFile The file where the token was found.
     * @param integer $stackPtr  The position in the stack where
     *                           the token was found.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        if (
            substr($tokens[$stackPtr]['content'], 0, 2) === '//'
            || substr($tokens[$stackPtr]['content'], 0, 1) === '#'
        ) {
            // Left trim: // style, # style and /* style comments are stripped, it happens that comments are commented.
            $content = trim(ltrim($tokens[$stackPtr]['content'], '/#* '));
            if (trim($content) !== '' && $this->isPhpCode($content)) {
                $error = 'Commented code is prohibited; found %s';
                $data = array(trim($tokens[$stackPtr]['content']));
                $phpcsFile->addError($error, $stackPtr, 'Found', $data);
            }
        }
    }

    /**
     * Checks if a string contains valid PHP code
     *
     * @param string $code The potential PHP code.
     *
     * @return boolean
     */
    private function isPhpCode(string $code): bool
    {
        // Open or close brackets when needed
        $openCount = substr_count($code, '{');
        $closeCount = substr_count($code, '}');
        if ($openCount > $closeCount) {
            $code .= str_repeat('}', $openCount - $closeCount);
        } elseif ($closeCount > $openCount) {
            $code = str_repeat('{', $closeCount - $openCount) . $code;
        }

        // Validate code
        $cmd = 'echo ' . escapeshellarg('<?php' . PHP_EOL . $code) . ' | php -ln';
        exec($cmd, $output, $retValue);
        return $retValue === 0;
    }
}
