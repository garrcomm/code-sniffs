<?php

namespace Garrcomm\Sniffs\Classes;

use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Files\File;

class ClassDeclarationInStringSniff implements Sniff
{
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return int[]
     */
    public function register()
    {
        return array(T_CONSTANT_ENCAPSED_STRING);
    }

    /**
     * Processes the tokens that this sniff is interested in.
     *
     * @param File    $phpcsFile The file where the token was found.
     * @param integer $stackPtr  The position in the stack where
     *                           the token was found.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $className = str_replace('\\\\', '\\', trim($tokens[$stackPtr]['content'], '"\''));

        // Skip empty classes
        if (trim($className, '\\') === '') {
            return;
        }

        // Try to autoload the class, to see if it exists.
        try {
            if (strpos($className, '\\') !== false && class_exists($className)) {
                $fix = '\\' . ltrim($className, '\\') . '::class';
                $error = 'Class reference found as string: %s. Use %s instead.';
                $data = array(trim($tokens[$stackPtr]['content']), $fix);
                if ($phpcsFile->addError($error, $stackPtr, 'Found', $data, 0, true)) {
                    $phpcsFile->fixer->replaceToken($stackPtr, $fix);
                }
            }
        } catch (\Throwable $throwable) {
            // It's possible the class_exists still throws an error, if the autoloaded file contains errors
            $warning = 'Class %s can\'t be autoloaded: %s';
            $data = array($className, $throwable->getMessage());
            $phpcsFile->addWarning($warning, $stackPtr, 'Found', $data);
        }
    }
}
