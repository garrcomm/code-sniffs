<?php

/**
 * This class is a drop-in replacement for vendor/squizlabs/php_codesniffer/tests/Standards/AbstractSniffUnitTest.php
 * but it's modified to run more or less stand-alone; relying on the PHPCS class itself did not work out that well.
 */

namespace Garrcomm\Tests;

use PHP_CodeSniffer\Config;
use PHP_CodeSniffer\Files\LocalFile;
use PHP_CodeSniffer\Reporter;
use PHP_CodeSniffer\Ruleset;
use PHP_CodeSniffer\Runner;
use PHPUnit\Framework\TestCase;

abstract class AbstractSniffUnitTest extends TestCase
{
    /**
     * Name of the sniff
     *
     * @var string
     */
    private $sniffName;

    /**
     * File that contains the test code
     *
     * @var string
     */
    private $testFile;

    /**
     * Reference to the PHP CodeSniffer Runner
     *
     * @var Runner
     */
    private $codeSniffer;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->sniffName = str_replace(['\\Tests\\', '\\'], ['.', '.'], substr(get_called_class(), 0, -8));
        $this->testFile = __DIR__ . substr(str_replace('.', '/', $this->sniffName), 8) . 'UnitTest.inc';

        // @TODO Maybe make this obsolete?
        $this->codeSniffer = new Runner();
        $this->codeSniffer->config = new Config([], false);
        $this->codeSniffer->reporter = new Reporter($this->codeSniffer->config);
        $this->codeSniffer->init();

        parent::setUp();
    }

    /**
     * Tests the sniff
     *
     * @return void
     */
    public function testSniff(): void
    {
        // Prepare config
        $config = new Config([], false);
        $config->standards = [__DIR__ . '/../'];
        $config->sniffs = [$this->sniffName];

        // Process file
        $file = new LocalFile($this->testFile, new Ruleset($config), $config);
        $file->process();

        // Assert error and warning count
        $this->assertType($this->getErrorList(), $file->getErrors(), 'error');
        $this->assertType($this->getWarningList(), $file->getWarnings(), 'warning');

        // Assert fixes
        $fixFile = $this->testFile . '.fixed';
        if (file_exists($fixFile)) {
            $file->fixer->fixFile();
            $this->assertEquals(file_get_contents($fixFile), $file->fixer->getContents(), 'Fix not successful');
        }
    }

    /**
     * Asserts of the result set contains the required items (and nothing more or less)
     *
     * @param array<int, int>        $list    The expected result.
     * @param array<int, \Countable> $results The actual result.
     * @param string                 $type    The type; 'warning' or 'error'.
     *
     * @return void
     */
    private function assertType(array $list, array $results, string $type): void
    {
        // Asserts if we have the correct amount of occurrences on each line
        foreach ($list as $line => $count) {
            $this->assertArrayHasKey(
                $line,
                $results,
                '0 ' . $type . '(s) found on line ' . $line . '. '
                . $count . ' ' . $type . '(s) expected.'
            );
            $this->assertCount(
                $count,
                $results[$line],
                count($results[$line]) . ' ' . $type . '(s) found on line ' . $line . '. '
                . $count . ' ' . $type . '(s) expected.'
            );
        }

        // Asserts if there are errors found that are not expected
        foreach ($results as $line => $value) {
            $this->assertArrayHasKey(
                $line,
                $list,
                count($value) . ' ' . $type . '(s) found on on line ' . $line . '. '
                . '0 ' . $type . '(s) expected.'
            );
        }
    }

    /**
     * Returns the lines where errors should occur.
     *
     * The key of the array should represent the line number and the value
     * should represent the number of errors that should occur on that line.
     *
     * @return array<int, int>
     */
    abstract protected function getErrorList(): array;

    /**
     * Returns the lines where warnings should occur.
     *
     * The key of the array should represent the line number and the value
     * should represent the number of warnings that should occur on that line.
     *
     * @return array<int, int>
     */
    abstract protected function getWarningList(): array;
}
