<?php

namespace Garrcomm\Tests\Namespaces;

// This is not in use in this file, although Bar\Exception is in use.
use Exception;

// This is in use in this file
use RuntimeException;

// Aliased class, in use in this file
use InvalidArgumentException as IAE;

// A namespace can also be aliased, not just a class name
use Foo\Bar as Bar;

// Use statement with leading backslash
use \OutOfRangeException;

// Multiple use statements at once
use \OverflowException, \RangeException as HasRange;

// This one is also not in use
use Baz\Bar as Foo;

// Not in use as well
use function Daz\Baz as Foobarbaz;

class UnusedUsesUnitTestClass
{
    // Use statements can also be traits
    use Bar\ClassTrait;
    // Traits can also be used with with aliased functions
    use FooBar\ClassTrait {
        method1 as methoda;
    }

    public function method1(): void
    {
        throw new RuntimeException();
    }

    public function method2(): void
    {
        throw new IAE();
    }

    public function method3(): void
    {
        throw new Bar\Exception();
    }

    public function method4(): void
    {
        $baz = '456';

        foobaz(function($bar) use ($baz) {
            echo $bar . $baz;
        });

        throw new OutOfRangeException();
    }
}
