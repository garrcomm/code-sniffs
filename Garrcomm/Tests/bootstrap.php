<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../../vendor/squizlabs/php_codesniffer/autoload.php';

// This constant is not taken care of properly in phpcs itself, so it's defined in the bootstrap
define('PHP_CODESNIFFER_CBF', false);
