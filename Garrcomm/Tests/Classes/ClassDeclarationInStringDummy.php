<?php

namespace Garrcomm\Tests\Classes;

/**
 * This class can't be autoloaded, although it complies with PSR-4 standards.
 * The reason is that it extends a non-existent class.
 */
class ClassDeclarationInStringDummy extends \Garrcomm\Tests\ButThisDoesntExist
{
}
