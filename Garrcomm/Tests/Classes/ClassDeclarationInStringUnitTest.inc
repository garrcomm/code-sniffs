<?php

// Classname of existing class with single quotes and backslashes not escaped
echo 'Garrcomm\Tests\AbstractSniffUnitTest';

// Classname of existing class with double quotes and backslashes not escaped
echo "Garrcomm\Tests\AbstractSniffUnitTest";

// Classname of existing class with double quotes and backslashes escaped
echo "Garrcomm\\Tests\\AbstractSniffUnitTest";

// Classname of non-existent class shouldn't be replaced
echo 'Garrcomm\Tests\ButThisDoesntExist';

// Classname of existent class with non-existent implementation, shouldn't be replaced
echo 'Garrcomm\Tests\Classes\ClassDeclarationInStringDummy';

// Root classes aren't replaced, they can easily be mistaken for words
echo 'Exception';

// Empty string or a single backslash should also be ignored
echo '' . '\\';
