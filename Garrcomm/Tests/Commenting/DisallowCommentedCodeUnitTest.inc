<?php

// Regular comments should be allowed
// So this is okay
// singlewordisalsookay
// butafunctionisnot();
// #1337 comments are legal code, but should not be blocked
// // butthismustbeblocked();
// In docblocks it should still be legal for example purposes
/**
 * var_dump();
 */
# butthismustbefixed();

// Multi-line codeblocks must also be alerted
// if ($foo == bar) {
//    var_dump($baz);
// }
