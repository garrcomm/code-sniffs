<?php

// Comments with slashes are allowed
// So this is okay

// Comment blocks are also allowed
/* so this is also okay */
/**
 * And this as well
 */

// But perl style comments are not allowed
# So this is an error

// Comments in comments are also tricky
// // This should be allowed
// #1337 this should be allowed as well
