<?php

namespace Garrcomm\Tests\Commenting;

use Garrcomm\Tests\AbstractSniffUnitTest;

class DisallowCommentedCodeUnitTest extends AbstractSniffUnitTest
{
    /**
     * Returns the lines where errors should occur.
     *
     * The key of the array should represent the line number and the value
     * should represent the number of errors that should occur on that line.
     *
     * @return array<int, int>
     */
    protected function getErrorList(): array
    {
        return [
            6 => 1,
            8 => 1,
            13 => 1,
            16 => 1,
            17 => 1,
            18 => 1,
        ];
    }

    /**
     * Returns the lines where warnings should occur.
     *
     * The key of the array should represent the line number and the value
     * should represent the number of warnings that should occur on that line.
     *
     * @return array<int, int>
     */
    protected function getWarningList(): array
    {
        return [];
    }
}
