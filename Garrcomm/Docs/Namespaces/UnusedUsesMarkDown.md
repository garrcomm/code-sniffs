# Garrcomm.Namespaces.UnusedUses.Found

This sniff removes unused `use` statements.

### When?

When a use statement like `use Foo\Bar;` is found, but not in use.

### Why?

This code can be removed safely. Many times they are left-overs when programming.
