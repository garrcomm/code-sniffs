# Garrcomm.Classes.ClassDeclarationInString.Found

This sniff replaces class names as string to `::class` constants.

### When?

When a string (only) contains a class name; for example: `$em->getReference('foo\bar', 1);`
This should be `$em->getReference(\foo\bar::class, 1);`

### why?

Multiple reasons; in development software it's possible to "Find usages" of a class or refactor a class name. When using the `::class` constant, the "Find usages" and "Refactor" results can be trusted more.
Also, a classname like `"foo\rar\naz"` will fail because `\r` will be replaced to a Return character and `\n` to a New line character.

