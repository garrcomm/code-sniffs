# Garrcomm.Commenting.DisallowHashComments.Found

This sniff makes comments more consisent.

*This sniff is actually based on the example shown at https://github.com/squizlabs/PHP_CodeSniffer/wiki/Coding-Standard-Tutorial
I made a small modification though; it also works with the code beautifier so code can be fixed automatically.*

### When?

When a comment like `# This is a Perl style hash comment` is found.

### Why?

I've added this sniff because it was part of the example, thus I learned from making this, but it also helps making comments more consistent.
