# Garrcomm.Commenting.DisallowCommentedCode.Found

This sniff forbids to push code that's commented out.

### When?

When a comment contains code which is, appairently, commented out: `// var_dump($foobar);`

It's also possible to have code examples in docblocks, so this sniff only responds on code commented out with `//` comments. A comment like this is still legal:
```
/**
 * @example
 *
 * echo foobar('baz');
 */
```

### Why?

To be able to maintain a history of code a file, it's best to use version control software.
Code that's commented out makes code unreadable and is considered garbage. This is explained in depth at https://kentcdodds.com/blog/please-dont-commit-commented-out-code
